" VERSION 2
"******** screens/screen_477/a/Resources.language ********"
      1 de   |Allgemein
      1 en   |General
      2 de   |Wählen
      2 en   |Select
      3 de   |EDI-Nachrichten Lieferant
      3 en   |EDI messages - Supplier
      4 de   |Konfigurationsprogramm
      4 en   |Configuration program
      5 de   |Abruf
      5 en   |Delivery schedule
      6 de   |Übertragungsnummer
      6 en   |Transfer number
      7 de   |Vorgangstyp
      7 en   |Process type
      8 de   |Aktiv
      8 en   |Active
      9 de   |Programm Plausibilitätsprüfung
      9 en   |Plausibility check program
     10 de   |EDI-Format
     10 en   |EDI format
     11 de   |Protokoll
     11 en   |Log
     12 de   |Druckformat
     12 en   |Print format
     13 de   |Verarbeitung
     13 en   |Processing
     14 de   |Schema
     14 en   |Schema
     15 de   |Nachricht
     15 en   |Message
     16 de   |Import/Export
     16 en   |Import/Export
     17 de   |Identnummer
     17 en   |Identity number
     18 de   |Suchwort
     18 en   |Search word
     19 de   |Bezeichnung
     19 en   |Description
     20 de   |Bemerkungen
     20 en   |Remarks
     21 de   |Lieferant
     21 en   |Supplier
     22 de   |Parameter für Sendedatum
     22 en   |Parameter for send date
     23 D    |Mit dem Turnus definieren Sie, mit welchem Rhythmus das Sendedatum berechnet werden soll.
     23 E    |The frequency of the send date.
     23 de   |Turnus
     23 en   |Frequency
     24 D    |Mit dem Zyklus definieren Sie, in welchem Intervall ein Turnus ausgefuehrt wird.
     24 E    |The interval of the frequency.
     24 de   |Zyklus
     24 en   |Cycle
     25 de   |Ultimo
     25 en   |Month-end
     26 de   |Startdatum
     26 en   |Start date
