" VERSION 2
"******** screens/screen_1/a/Resources.language ********"
      1 de   |Slowakisch
      1 en   |Slovakian
      2 de   |Griechisch
      2 en   |Greek
      3 de   |Individuelle Sprache 1
      3 en   |1st individual language
      4 de   |Mazedonisch
      4 en   |Macedonian
      5 de   |Tschechisch
      5 en   |Czech
      6 de   |Bulgarisch
      6 en   |Bulgarian
      7 de   |Italienisch
      7 en   |Italian
      8 de   |Website besuchen
      8 en   |Visit website
      9 de   |Übersetzen
      9 en   |Translate
     10 de   |Englisch
     10 en   |English
     11 de   |Individuelle Sprache 3
     11 en   |3rd individual language
     12 de   |Slowenisch
     12 en   |Slovenian
     13 de   |Serbisch
     13 en   |Serbian
     14 de   |EDI
     14 en   |EDI
     15 de   |Ungarisch
     15 en   |Hungarian
     16 de   |Allgemeines
     16 en   |General
     17 de   |Russisch
     17 en   |Russian
     18 de   |Bezeichnung
     18 en   |Description
     19 de   |Deutsch
     19 en   |German
     20 de   |Portugiesisch
     20 en   |Portuguese
     21 de   |Rumänisch
     21 en   |Romanian
     22 de   |Indonesisch
     22 en   |Indonesian
     23 de   |Französisch
     23 en   |French
     24 de   |Arabisch
     24 en   |Arabic
     25 de   |Chinesisch vereinfacht
     25 en   |Chinese,simplified
     26 de   |E-Mail senden
     26 en   |Send email
     27 de   |Individuelle Sprache 2
     27 en   |2nd individual language
     28 de   |Thailändisch
     28 en   |Thai
     29 de   |Lieferant
     29 en   |Supplier
     30 de   |Polnisch
     30 en   |Polish
     31 de   |Persisch
     31 en   |Persian
     32 de   |Versand, Warenverkehr
     32 en   |Shipping, Goods traffic
     33 de   |Spanisch
     33 en   |Spanish
     34 de   |Vietnamesisch
     34 en   |Vietnamese
     35 de   |Niederländisch
     35 en   |Dutch
     36 de   |Türkisch
     36 en   |Turkish
     37 de   |Chinesisch traditionell
     37 en   |Chinese,traditional
     38 de   |Urdu
     38 en   |Urdu
     39 de   |Amerikanisches Englisch
     39 en   |American English
     40 de   |Merkmale
     40 en   |Characteristics
     41 de   |Seite 1
     41 en   |Page 1
     42 de   |Koordinaten
     42 en   |Coordinates
     43 de   |Anschrift Bestellung
     43 en   |Sales order address
     44 de   |Anschrift Versand
     44 en   |Shipping address
     45 de   |Längengrad
     45 en   |Longitude
     46 de   |Breitengrad
     46 en   |Latitude
     47 de   |DMS
     47 en   |DMS
     48 de   |Letztes Dokument
     48 en   |Last document
     49 de   |Öffnen
     49 en   |Open
     50 de   |Dokumente archivieren
     50 en   |Archive documents
     51 de   |Belegart
     51 en   |Document type
     52 de   |Barcode
     52 en   |Barcode
     53 de   |Zuordnen
     53 en   |Allocate
     54 de   |Betreff
     54 en   |Subject
     55 de   |Datei
     55 en   |File
     56 de   |Japanisch
     56 en   |Japanese
     57 de   |Lieferantenakte
     57 en   |Supplier file
     58 de   |Business App Lieferantenakte neu laden
     58 en   |Reload Supplier file Business App
     59 de   |Business App Dokumente neu laden
     59 en   |Reload Documents Business App
     60 de   |Identnummer
     60 en   |Identity number
     61 de   |Suchwort
     61 en   |Search word
     62 de   |Text 1
     62 en   |Text 1
     63 de   |Text 2
     63 en   |Text 2
     64 de   |Editieren
     64 en   |Edit
     65 de   |Name
     65 en   |Name
     66 de   |Straße
     66 en   |Street
     67 de   |Postleitzahl
     67 en   |Postcode
     68 de   |Ort
     68 en   |Town
     69 de   |Region
     69 en   |Region
     70 de   |Land
     70 en   |Country
     71 de   |Telefon
     71 en   |Phone
     72 de   |Anrufen
     72 en   |Call
     73 de   |Mobiltelefon
     73 en   |Mobile phone
     74 de   |Fax
     74 en   |Fax
     75 de   |E-Mail
     75 en   |Email
     76 de   |Senden
     76 en   |Send
     77 de   |Website
     77 en   |Website
     78 de   |Besuchen
     78 en   |Visit
     79 de   |Anrede
     79 en   |Salutation
     80 de   |Ansprechpartner
     80 en   |Contact person
     81 de   |Abteilung
     81 en   |Department
     82 de   |Kennzeichen
     82 en   |Code
     83 de   |Bemerkungen
     83 en   |Remarks
     84 de   |EDI-Nachrichten
     84 en   |EDI messages
     85 de   |Kunden-Lieferanten-Beziehung
     85 en   |Customer/Supplier relation
     86 de   |Spediteur
     86 en   |Forwarder
     87 de   |GLN
     87 en   |GLN
     88 de   |Rechnung
     88 en   |Invoice
     89 de   |Betreuer
     89 en   |Inhouse contact
     90 de   |Sprache
     90 en   |Language
     91 de   |Vorgangssteuerregel
     91 en   |Process tax rule
     92 de   |Währung
     92 en   |Currency
     93 de   |Bruttopreise
     93 en   |Gross prices
     94 de   |Landart
     94 en   |Country type
     95 de   |Steuernummer
     95 en   |Tax number
     96 de   |EU-Kennung
     96 en   |EU code
     97 de   |Umsatzsteuer-Identifikationsnummer
     97 en   |VAT registration number
     98 de   |DUNS
     98 en   |DUNS
     99 de   |Kundennummer
     99 en   |Customer number
    100 de   |Preisgruppe
    100 en   |Price group
    101 de   |Rabattgruppe
    101 en   |Discount group
    102 de   |Preisstellung
    102 en   |Pricing
    103 de   |Rechnungsstellung
    103 en   |Invoicing
    104 de   |Gewährleistung
    104 en   |Warranty
    105 de   |Lieferbedingung
    105 en   |Delivery terms
    106 de   |Code
    106 en   |Code
    107 de   |Zahlung
    107 en   |Payment
    108 de   |Zahlungsbedingung
    108 en   |Terms of payment
    109 de   |Zahlungsbedingungsschlüssel
    109 en   |Terms of payment code
    110 de   |Zahlungsart
    110 en   |Method of payment
    111 de   |Zahlungssperre
    111 en   |Payment lock
    112 de   |Skontokarenztage gesperrt
    112 en   |Cash discount grace period locked
    113 de   |SEPA UCI
    113 en   |SEPA UCI
    114 de   |SEPA-Lastschriftmandat
    114 en   |SEPA direct debit mandate
    115 de   |Bankverbindung
    115 en   |Bank details
    116 de   |Bearbeiten
    116 en   |Edit
    117 de   |Neue erzeugen
    117 en   |Generate new
    118 de   |Bankname
    118 en   |Bank name
    119 de   |Internationale Ident
    119 en   |International ID
    120 de   |Nationale Ident
    120 en   |National ID
    121 de   |IBAN
    121 en   |IBAN
    122 de   |Bankkontonummer
    122 en   |Bank account number
    123 de   |Kontoinhabername
    123 en   |Name of account holder
    124 de   |Liquiditätsplanung
    124 en   |Liquidity planning
    125 de   |Parameter aktiv
    125 en   |Active parameter
    126 de   |Fälligkeit der offenen Posten
    126 en   |Due date outstanding items
    127 de   |Karenztage zwischen Lieferschein und Rechnung
    127 en   |Grace period between delivery note and invoice
    128 de   |Konsignationslagerplatz
    128 en   |Consignment warehouse location
    129 de   |Mahnsperre
    129 en   |Reminder lock
    130 de   |Intrastat
    130 en   |Intrastat
    131 de   |Zollkennzeichen
    131 en   |Customs tariff number
    132 de   |Art des Geschäfts
    132 en   |Nature of business
    133 de   |ATLAS
    133 en   |ATLAS
    134 de   |Teilnehmer-Identifikationsnummer
    134 en   |Participant identification number
    135 de   |Angemeldetes Verfahren
    135 en   |Registered procedure
    136 de   |Vorangegangenes Verfahren
    136 en   |Previous procedure
    137 de   |Weiteres Verfahren
    137 en   |Further procedure
    138 de   |Beförderungsmittel im Inland
    138 en   |Inland means of transport
    139 de   |Verkehrszweig
    139 en   |Mode of transport
    140 de   |Beförderungsmittel an der Grenze
    140 en   |Means of transport at border
    141 de   |Art
    141 en   |Type
    142 de   |SLP-Status
    142 en   |CS status
    143 de   |Unternehmens-Identifikationsnummer
    143 en   |Company identification number
