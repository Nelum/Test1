package com.nelumdev.abas.epay;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.exception.CommandException;
import de.abas.erp.db.schema.custom.bulkepaymentboc.EPaymentBOC;
import de.abas.erp.db.schema.custom.bulkepaymentboc.EPaymentBOCEditor;
import de.abas.jfop.base.buffer.BufferFactory;
import de.abas.jfop.base.buffer.GlobalTextBuffer;

public class FileHandler {
	
	public static void createFile(DbContext ctx,EPaymentBOCEditor head) throws IOException {
		HashMap<String,String> systemData = new HashMap<String,String> ();
		systemData  = getGlobalData();
		String file = "./tmp/" + "EPAY_" + systemData.get("SDATE2")+ systemData.get("STIME2") + ".dat";
		file = file.replaceAll(":", "");
		String hashcode  = systemData.get("SDATE2")+ systemData.get("STIME2").replaceAll(":", "");
		String delimiter = head.getString("yepaymsett^yepaymdelim");
		try{
			writeDataToFile(head, file, hashcode, delimiter);
			head.setYepaymfiledone(true);
			head.setYepaymfilename(file);
			validateFileExist(ctx, head);
			updateRows(head);
		} catch (IOException e) {
			e.printStackTrace();
			TextBox textBox = new TextBox(ctx,"Error","Something went wrong");
			textBox.show();
		}
	
	
		
	}

	private static void writeDataToFile(EPaymentBOCEditor head, String file, String hashcode, String delimiter)
			throws IOException {
		FileWriter fileWriter = new FileWriter(file);
		String headerText     = head.getString("yepaymsett^yepaymhprefix") + delimiter + head.getString("yepaymsett^yepaymname") + delimiter + 
				head.getString("yepaymsett^yepaymcode");
		fileWriter.write(headerText);
		Iterable<EPaymentBOC.Row> rows = head.table().getRows();
		for(EPaymentBOC.Row row : rows ){
			String body = "\nB" + delimiter + row.getString("yepaymacc") + delimiter +row.getString("yepaymaccno") +
					delimiter + row.getString("yepaympayamt") + delimiter + row.getString("yepaymemail")+ delimiter +row.getYepaympaydes();
			fileWriter.write(body);
		}
		fileWriter.write("\nF" + delimiter +  hashcode);
		fileWriter.close();
	}

	public static HashMap<String,String> getGlobalData(){
		BufferFactory bufferFactory  = BufferFactory.newInstance(true);
		GlobalTextBuffer globalText  = bufferFactory.getGlobalTextBuffer();
		HashMap<String,String> gdata = new HashMap<String,String>();
		gdata.put("OPERATOR", globalText.getStringValue("operatorCode"));
		gdata.put("SDATE", globalText.getStringValue("date"));
		gdata.put("SDATE2", globalText.getAbasDateValue("date").toString());
		gdata.put("STIME", globalText.getStringValue("currTime"));
		gdata.put("STIME2", globalText.getStringValue("currTime").toString());
		return gdata;
	}

	public static void updateRows(EPaymentBOCEditor head) {
		Iterable<EPaymentBOCEditor.Row> updaterows = head.table().getEditableRows();
		for(EPaymentBOCEditor.Row urow : updaterows){
			urow.setYepaymstatus("icon:ok");				
			try {
				head.commitAndReopen();
			} catch (CommandException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void validateFileExist(DbContext ctx, EPaymentBOCEditor head) {
		try {
			head.commitAndReopen();
		} catch (CommandException e) {
			e.printStackTrace();
			TextBox textBox = new TextBox(ctx,"Error","Something went wrong");
			textBox.show();
		}
	}
	
	
}
