package com.nelumdev.abas.epay;

import de.abas.erp.axi2.event.Event;

public class ScreenMode {
		
		public static boolean isNew(Event event){
			return event.getCommand().getDisplayString().equals("New");
		}
		
		public static boolean isEdit(Event event){
			return event.getCommand().getDisplayString().equals("Edit");
		}
		
		public static boolean isView(Event event){
			return event.getCommand().getDisplayString().equals("View");
		}
}
